# Hiram: The American Civil War's Western Theater, 1862-1863

Rules version 0.7.0

## Index
* Components: @sec:components
* Victory: @sec:victory
* Map: @sec:map
* Tactical Cards: @sec:tc
* Command Cards: @sec:cc
* Pieces: @sec:pieces
* Sequence of play: @sec:play
* Combat: @sec:combat
* Supply: @sec:supply
* Setup: @sec:setup
* Playtest: @sec:playtest

##  Components {#sec:components}
* 1 map
* 1 counter sheet
* Not included but needed for play: 4 decks of cards.
* scratch paper (not included) for keeping track of how many divisions are in each General's army

### Counter sheet contents
* Generals
* Cavalry
* Supply Trains
* VP Markers, and other markers for General records keeping

## Foreword {#sec:fwd}
The setting for this game is the American Civil War (ACW) in a portion of the western theater, beginning around February or March of 1862. One player takes the role of the Confederate States of America (CSA), the other the United States of America (USA).

This game employs a rift of the system developed by Richard Sivel in his excellent designs *Friedrich* and *Maria*. The variation of the Sivelian system used in *Hiram* adds a few extra nuances but should be possible to play to conclusion in a single session.

# Victory Conditions & Game Length {#sec:victory}

The USA player immediately wins as soon as they control 11 of the 14 objective cities. If the game ends before the USA player meets their victory condition, the CSA player wins.

At the beginning of the game, the game is limited to 8 turns. Certain conditions will affect the game length, and when these occur the game is decreased or increased by a single turn. The maximum turn limit in the game is 10.

## Conditions that shorten the game

* Any time the CSA player captures or recaptures ('liberates') an objective city, **or** wins a major victory, the turn limit marker is reduced by one turn.

* If a player did not initiate an attack during the entire turn (all three maneuver rounds) they are penalized: the USA gets penalized with a one turn decrease, the CSA with a one turn increase.

* Whenever the USA player introduces a level-3 skilled General onto the map, and the USA has at least two level-3 skilled Generals already on the map, the game length is decreased by one turn.

*Example: in a Maneuver round, Grant and Thomas are on the map, the USA player fires Pope for losing a major battle and replaces him with McPherson so the game is immediately shortened from 8 turns to 7 turns. In the Recovery Round of that turn, the USA player brings Sherman on to the map as well which would be the fourth skill level-3 General for the USA, so the game length is immediately reduced again by a single turn, from 7 turns to 6 turns.*

## Conditions that lengthen the game

* If the USA player wins a major victory, the turn marker limit is increased by one turn. *NOTE:* The game length is not extended when the USA captures/recaptures an objective.

* If the USA player introduces a level-0 skilled General on to the map the game is increased by one turn.

* Any time there are **three** CSA cavalry units in the cavalry screen box, the game length is increased by one turn.

# The Map {#sec:map}
The map consists of cities connected by lines of communication along which the pieces move. There are three types of lines of communications: road, rail, and riverine. Some of the cities are also objectives, which the players vie for control of in order to win the game. The map grid corresponds to different suits of cards, which determine the trump suit for combat.

* Cities may be neutral, friendly, or enemy/hostile. This status of cities is depicted by their symbol. Cities depicted with blue circles are friendly to the USA, cities of orange squares are friendly to the CSA, and cities of white triangles are neutral. The colored surfaces of the map correspond to these colors.

* Cities friendly to the USA are hostile to the CSA and vice versa.

# Tactical Cards (TC) {#sec:tc}
TC are the game's currency: players use TC to conduct combat, avoid combat (see cavalry screens), or recruit troops.

Every TC has a suit (hearts, clubs, diams, spades) and a value from 2 to 10.

There is a special wild card type called "Reserve". When a player plays a Reserve card, he declares it as being any suit and any value from 1 to 8. _NOTE:_ Declaring the value is of great use in minimizing battle losses.

# Command Cards {#sec:cc}
Each player has a deck of command cards that they use for activating units and in some cases for conducting combat. 

Each General has one command card for each skill level, so a General with a skill rating has three coressponding command cards while a General of zero skill has no command cards. 

Command cards corresponding to Generals can be used for the following actions: 

* Moving the General

* Moving a friendly General. This friendly General must be no more than six cities away from the General whose command card was played

* Moving any friendly supply train located on the board 

* Being used as 4 TC wild card in combat if the combat occurs within six cities of the General whose cmmand card was played

* Purging one low morale card from the player's hand -- this is returned to the supply, not the discard pile. 

There are also three other types of command cards: low morale, cavalry, and strategic command. 

Each player has one strategic command card in their deck, these can be used to move any general or supply train on the map, or as a 2 TC wild card in combat.

Each player has cavalry cards corresponding to the number of cavalry units they own: one for the USA and two for the CSA. Cavalry command cards are used for cavalry screens, placing/moving cavalry onto the map, or recalling cavalry units from cavalry screen box. 

Low morale cards are junk cards that serve no purpose, these are accumulated from losses in combat or low-supply situations. 

The USA player has a hand limit of six command cards and the CSA player has a hand limit of five command cards. 

The cards in the player's command deck correspond to the generals actively in play at that time. If a General is removed or added to the map, the players also add or remove the command cards corresponding to that General to their command card deck. 

Any time a player uses or discards a command card they place it on their discard pile. Players are only allowed to use the command cards in their hand, not their discard pile or draw deck. 

During maneuver rounds, a player may play or discard (without effect) some, none or all of their command cards, then refill to their hand limit at the end of the maneuver round. If the player expends no command cards they effectively are passing that round. 

Whenever the draw deck is empty, shuffle the discard pile to form a new draw deck. 

# Pieces {#sec:pieces}
There are three types of pieces in the game: Generals, Cavalry, and Supply Trains.

## Generals
Players move and fight with their Generals. Each player has a supply of Generals, some of whom start on the board and the other in the Available Generals box.

* A General may be discarded from the game, either by being fired, or when they cannot satisfy retreat requirements, or in the case of skill level 0 Generals only, when they lose a battle. When this happens the General is removed from the map and is not available for play for the remainder of the game.

* Generals have a skill rating of 0 to 3.

* The USA is limited to 7 Generals in play, the CSA player is limited to 6. 

* Each General must command at least one division.

* The maximum number of divisions a USA General may command is 10, and the maximum a CSA General may command is 8.

### Stacks of Generals
  * Normally, only one General may ever be placed on each city. **Exception:** Two Generals may occupy one city to form a stack. In this case, the General with the higher skill rating is the overall commander and is placed on top of the stack. The overall commander's skill rating is used for combat and movement purposes. If two Generals have the same skill rating, the player decides which General is the overall commander.

  * When a player forms a stack, movement is finished that maneuver round for both Generals that form the stack.

  * In a stack, Generals treat the sum of their divisions as a common value. Each stack must contain at a minimum two divisions, with a maximum of 20 divisions for a stack of USA Generals or 16 divisions for a stack of CSA Generals.

  * As long as his Generals are stacked, a player can transfer brgiades between his Generals whenever he desires. He can do it even during the active turn of the other other player.

  * A General who loses his last division is removed from the board, unless the General is in a stack and it is possible to transfer at least one troop to him.

  * Generals may dissolve the stack at the beginning of the movement phase and move separately.

### How Generals Move
* Generals move along the board from city to city using a line of communication - either rail, road, or river. A General may move up to the movement allowance, even back and forth.

* A General can only utilize one type of movement rate, it cannot mix different movement rates. For example, Generals may only use railway movement if the entirety of the move is conducted on a railroad, i.e., they start and stop movement on a railroad and likewise, USA Generals may only use river movement if beginning and ending on a river space.

* Generals can move up to four cities by road, or eight cities by rail. Also, only one USA General per maneuver phase mave move up to eight cities by river. **Exception:** Generals on either side with a skill rating of zero can only use half of the normal movement allowance for all the different types of movement.

* **CSA Generals may move an additional city if** both of the following conditions are met:
  1. The cities along the line of movement are all friendly
  2. The cities along the line of movement do not include enemy controlled objectives

* Generals may never move onto or through enemy Generals or friendly supply trains.

* Generals may move onto an enemy cavalry unit and or enemy supply train and continue moving. If a General moves onto an enemy supply train it is considered to be captured, that supply train is removed from the board, and the player who captured it immediately gets a bonus draw of a single TC.

#### The Mighty Mississippi
* The Mississippi River is an obstacle that may only be crossed at the ten locations designated on the map.

* Crossing the Mississippi consumes the entire movement allowance of a General or a Supply Train. To cross the Mississippi, a General or Supply Train must begin adjacent to the Mississippi. Upon reaching the far side after crossing the Mississippi, the  General or supply Train ceases movement for the duration of the maneuver phase.

* Pieces on opposite sides of the Mississippi are not adjacent.

* Supply trains cannot throw supply across the Mississippi River except at the ten designated crossing points. 

* _NOTE_: St. Louis does not have a Mississippi connection, so it and its connections are treated normally, and the above rules concerning adjaceny, movement, and supply do not apply to it.

### Conquering objectives

* Control of objectives changes due to conquest. Only a face-up General may conquer an objective. An objective is conquered when:
  * A face-up General moves out of the objective either by moving through it or by starting its move on it and moving away and
  * ...the objective is not protected at that moment.

* An objective is protected if a General of the side currently in control of the objective is positioned within 3 cities away.

* A General can conquer more than one objective in a single move. After conquest, a conquered objective may still be entered by other pieces.

* When an objective changes control, the conquering player must expend one TC of any value face up and then flip the marker accordingly to denote its new owner.

* **Retroactive Conquest.** If a General moves through or away form a protected objective, put a marker on it to denote its possible conquered status. In the retroactive conquest phase of the same stage, check each objective with such a marker. If the objective is not protected anymore (due to retreats after combat), it is retroactively conquered. If the objective is still protected it is not conquered and the marker is removed.

## Cavalry
The CSA player has three cavlary units and the USA player has two cavlary units. Cavalry units can block enemy supply traces, raid enemy supply trains, and can enable a General to refuse combat, which is known as a cavalry screen.

* Cavalry units do not move or participate in combat and cavalry units do not have to be in supply.

* Generals (but not supply trains) may move through enemy cavalry units. If an opposing General moves onto the enemy cavalry unit, that cavalry unit is simply returned to its owner (available for play in his next maneuver round) and the General may continue moving as usual.

* Friendly Generals and friendly supply trains may move through friendly cavalry units.

* Blocking supply: The players may place their cavalry units at the end of each of their movement phases by expending a cavalry command card from their hand. Cavalry units are placed on a vacant city up to 4 cities away by any combination of rail or road connections from a friendly General. If a player does not have a cavalry command card in their hand they may not place cavalry, additionally, a player cannot place cavalry on to the map that are already in the cavalry screen box, they may however effectively move cavalry already on the map to a new location.

* Raiding supply trains: If at any time during the active player's maneuver round that one of their Generals is within four cities of a an enemy supply train, and the active player has a cavalry command card in hand, they may expend it to cause their opponent to discard one TC from their hand faceup into the discard pile. This TC is chosen at random. 

* Players may not trace supply through a city that is occupied (also known as 'interdicted') by an enemy cavalry unit nor through all other cities one road or one rail movement away.

## Supply Trains
Supply Trains provide supply to Generals when they are in neutral or enemy territory.

Supply Trains move at a rate of 3 by road or 4 by rail, they may not utilize river movement.

Supply trains may not move onto or through a space occupied by a friendly or enemy General or enemy cavalry. They may however move through a space occupied by a friendly cavalry.

# Sequence of play {#sec:play}
Each turn consists of three maneuver rounds and one recovery round.

## Maneuver Rounds
The player with the most TC chooses if the CSA will go first in the maneuver phase and the USA second, or vice versa. In the event of a tie of TC, the player with the most objectives currently under his control chooses who goes first, and if still tied the CSA player chooses who goes first.

Each maneuver round consists of a simultaneous supply check then a movement-cavalry-combat round.

After a supply check, the active player reclaims cavalry from the board, conducts all movements, places cavalry, then conducts combats, and after these are resolved play passes to the next player who does the same.

After both players have acted in the maneuver round, the round is is over and players advance the marker to the next space on the round track.

**At the beginning of each player's maneuver round, if the player has no TC remaining, the player draws 1 TC.**

## Recovery Rounds
The Recovery round is an administrative round that both players conduct simultaneously. There is no movement or combat in this round.

These are the components of the recovery round:

* Draw TC
* Place Reinforcements
* Purchase supply trains and divisions
* Replace one General

### Draw TC
The **USA player draws nine TC** and discards one, the **CSA player draws eight TC** (unless modified, see below) and discards one.

When the players exhaust their first deck, they then draw from the next deck. When both decks are exhausted they reshuffle each deck individually and repeat the process of drawing from one deck at a time.

#### Negative TC Draw Modifiers for the CSA player
During the recovery round, the CSA player may suffer a TC draw penalty under the following cirucmstances:

* If the USA controls both Vicksburg and Memphis the CSA player draws two less TC.

-or-

* If the USA player controls all objectives in Tennessee (Knoxville, Nashville, Memphis, and Chattanooga), the CSA player draws two less TC.

### Place reinforcements
Each turn the CSA player receives two divisions and the USA player receives three divisions. Players keep track of these reinforcements on a sheet. These reinforcements may be used to add new Generals to the map, or to reinforce a General currently on the map, provided that General is not at the maximum division limit and that the General is face up (not out of supply).

### Buy divisions and supply trains
Aside from the reinforcements, each player may purchase additional divisions or supply trains (collectively referred to here as units) for 5 TC each. There is no change for purchases, i.e. a TC with a value of 9 can only purchase one unit, but a TC or 9 and 6 would be able to purchase 3 units.

A player may expend some, none, or all of his TC to purchase units.

Newly purchased divisions are placed in the same manner as reinforcements. Newly purchased supply trains can only enter the board in a vacant city adjacent to or on a vacant objective city that is currently friendly controlled.

### Replacement/Introduction Procedure for Generals

Whenever a player must add a General to the map, either by forming a new army or firing an existing General (see below), the players randomly select two Generals from the supply of their avialable Generals and choose one, the other is returned to the supply.

* The CSA player **must** select the General with the lower skill rating, returning the higher skilled General to their supply.
* The USA player **may** select the General with the higher skill rating, returning the other General to the supply.

In the case of two Generals with equal skill ratings, the players choose which to keep and which to return to the supply.


#### Firing Generals
Players may 'fire', i.e. replace, any one General on the map during the Recovery Phase. The General being replaced does not need to be in supply.

After taking the replacement, discard the replaced General from the game.

The replacing General takes the same position on the map as the replaced General and assumes control of all of the replaced General's divisions.

#### Introducing additional Generals to the map

The players have a limited amount of Generals that may be on the map at any one time, for the USA player this limit is 7 Generals, and for the CSA player it is 6 Generals. If players have not met this limit they may inroduce a new General to the map during the Recovery Phase.

Newly arrived Generals can only enter the board in a vacant city adjacent to or on a vacant objective city that is currently friendly controlled.

# Combat {#sec:combat}

* Every General who is adjacent to an enemy General at the beginning of his combat phase must attack. If more than one attack must be made the attacking player chooses the order of resolution. **Note**: Armies are never considered adjacent by riverine connections, they are only adjacent by road and rail connections.

* An attack is resolved as a card game using Tactical Cards (TC). A player may play only those TCs which are of the same suit as the sector in which **his** General is positioned.

* If a General/stack starts the combat adjacent to more than one opponent he has to fight them one after the other. If more than one General/stack are adjacent to one oppoent, they have to attack one after the other.

* In very rare circumstances, opposing Generals may end adjacent to each other yet neither player has any TC remaining. In this case, no combat occurs.

## Combat sequence
1. Cavalry screens
2. Determine the suit
3. Determine Initial Score
4. Determine if there is a TC bonus draw
5. Play TC
6. Resolve victory and retreats
7. Check for retroactive conquests

### (1) Cavalry screen
Either player may elect to retreat prior to combat by using a cavalry screen. 
To do so the player must have a cavlary card in their hand and have one available cavalry unit to expend.
The player discards their cavalry card and moves the cavalry unit to the cavalry screen box then moves their General(s) two cities away and the battle is over. Unlike a retreat, these cities are both chosen by the owner.
Additionally, in the case of the CSA player, if there is already a cavalry unit in the cavalry screen box, they must expend a TC of any value face up into the discard pile in order to pay for this second cavalry screen. 
The players do not draw new command cards to replace the cavalry cards used for cavalry screens.

* The General withdrawing using a cavalry screen may move in any direction provided he does not end adjacent to either the General or the city he withdrew from.

* During a withdrawal via a cavalry screen the defender may not conquer objectives or capture enemy supply trains.

* If the defender did not elect to use a cavalry screen, the battle resolution continues to step 2 - determining the suit.

### (2) Determining the suit
Each General can only conduct combat with cards matching the suit of the grid square in which the General is currently located.
If Generals are conducting combat across a grid square, they will each conduct combat in a different suit from each other.

### (3) Determine initial score
First the opposing players state how many divisions their participating Generals command. The difference between these two numbers is called the *initial score*. This score is negative for the player inferior in divisions and positive for the other player.

**Vicksburg** -- CSA Generals occupying Vicksburg in a defensive battle begin the battle with an additional +8 to their initial score. *This represents the inherent fortifications of Vicksburg.*

### (5) Play TC
* Next, the inferior player has the right to play a single TC of his suit. He adds the TC value to the initial score. This results in the new score which he states aloud. The score is always the same for both players, except that it is negative for one player and poisitive for the other. As long as a player has a negative score, he has the right to play another TC.

* If the player receives the right to play a TC on a score of zero, me must play card if he has any of the correct suit (he is not obligated to play a Reserve). If he has none (and he is not willing to play an existing Reserve), combat ends as a tie. In a tie, neither side loses divisions nor has to retreat.

* Stacked Generals always fight together.

* If a score becomes *zero or positive*, the right to play TCs switches to the other player. Now he is considered inferio and he may play a TC using the same procedure. The right to play TCs keeps switching until the player with the right to paly is unable or unwilling to do so. At that point his General is defeated.

* If the attack starts with a score of zero, the attacking player plays TC first.

* The defeated General loses as many divisions as the final *negative score* but no more than he commanded and retreats four spaces. Retreats apply to *defeated* Generals, whether they attacked or defended.

* The loser adds a low morale card to their discard pile. 

* The winner loses no divisions and may either remain in place *or* advance to the space the retreating General vacated. A victorious defending General may also make this advance after combat.

### (6) Resolve victory and retreats
* If a General loses all of his divisions as a result of combat, the General is removed from the board and returned to the supply of available Generals **unless he has a skill level of zero** in which case the General is removed from play.

* A General who had to rereat may not attack or be attacked again in that combat phase.

* A defeated General has to retreat before the next attack is resolved.

* During a retreat a stack may never split up.

* **Retreat Path**: A General retreats a number of cities equal to the number of losses sustained in combat. Both players determine where the defeated General retreats. The first, third, fifth, etc. cities are chosen by the victor, the second, fouth, sixth, etc. cities are chosen by the loser. _Example: Rosecrans of the USA is defeated in battle by a total of three, Rosecrans loses three divisions, then the first city he retreats to is chosen by the CSA player, then the USA player chooses there an adjacent city to retreat to, and finally the CSA player chooses the third city Rosecrans must retreat into_.

* A General may not pass through any individual city more than once during a retreat.

* Generals may not retreat through enemy Generals, enemy supply trains, or friendly supply trains, nor may they retreat across the Mississippi River.

* If a General cannot retreat the full distance due to occupied cities or a barrier such as the map edge or Mississippi River, he loses all divisions and is placed in the Relieved of Command box.

* **Major Victories**: If one side wins by a final TC score of 6 or more, *or* if a General with a skill level of 0 defeats a General with a skill level of 3, then the winning player achieves a "Major Victory". When this occurs, players adjust the turn length accordingly, lengthing the game by one turn if the USA won, or reducing the game length by one turn if the CSA won. Additionally, the player who loses a major victory may immediately fire their General who lost the major victory. Finally, the player who lost a minor victory adds two low morale cards to their discard pile. 

### (7) Check for retroactive conquests
Retroactive conquests are checked for after all combat is executed.

# Supply {#sec:supply}
Generals need supply. Generals check for supply at the beginning of each maneuver phase. If a General is not situated on a friendly city it must be able to draw supply from a valid supply source.

## Supply sources

* Supply trains: A supply train is a source of supply if it can draw supply from a friendly city that is not an enclave, or another friendly supply train that likewise satisfies these conditions. In this way, players may chain together multiple supply trains to form a network of supply lines.

## Supply Lines
Supply trains and Generals can **draw supply from a supply train up to 5 cities away** - this forms a supply line.

A supply line may combine road or rail movement rates only (with one exception for the USA player, see below). Supply lines cannot extend through enemy occupied cities (enemy cavalry, General, or Supply Trains) **and** may also not extend through unoccupied objective cities that have an enemy marker on them.

**All USA Generals are also in supply if located on a city on a river that can trace a riverine path of unlimited distance to Cairo, St. Louis, or Louisville provided that no intervening cities in this path are occupied by a CSA General or CSA cavalry unit.**

## Enclaves
Friendly cities located in predominantly enemy or neutral areas are known as enclaves.

* USA enclaves:
  * Oneida, TN
  * Greenville, TN
  * Haleyville, AL
  * Laurel, MS
  * Marhsall, AR

* The CSA has a single enclave in Henderson, KY.

Players may not introduce new pieces on the map onto enclaves.

**Note**: Enclaves may supply friendly Generals located there, but they cannot be used as a draw source for a supply train.

## Out of supply (OOS)
If a General cannot trace a line of supply to a friendly supply train, or is not located on a friendly city, then it is considered to be out of supply.

* If a player's supply line is blocked by an enemy cavalry unit at the beginning of his maneuver phase he may pay a single TC, worth any amount to keep his General in supply.

* When a General cannot trace a supply path when required, he immediately runs out of supply, is turned face up, adds one low morale card to his discard pile, then faces two options: either 1) lose one division from his army or 2) expend a TC of any value to not take this loss.

* If a stack of two generals is out of supply the stack either 1) loses two divisions or 2) its owner expends two TC of any value to not take losses.

* Generals who are out of supply move as normal and fight as normal but may not capture objectives.


## Capturing supply trains
Supply trains may be captured by enemy Generals.

To capture an enemy supply train, players simply move Generals onto them.

This stops the movement of the General and this removes the supply train from the board, not permanently, it may be purchased and placed again in the next recovery round.

The capturing player then earns a bonus: the player draws a single TC per enemy supply train captured.

# Set up {#sec:setup}
Place the turn limit marker on turn 8 and the current turn marker on turn 1.

Prepare the TC draw deck: Take 4 decks of cards, give each player two decks. Each player has their own deck.

Remove the Jokers, and all the face cards except for the Aces. Then both players shuffle each deck individually.

The CSA player draws 6 TC and discards one, the USA player draws 7 TC and discards one.

Prepare the command card deck: The USA player takes the command cards for Grant, Thomas, and Buell as well as the cavalry and strategic command card, shuffles them and draws six to form their initial hand. The CSA player takes the command cards for AS Johnston, Bragg, Crittenden, and K. Smith as well as their two cavalry cards and their strategic command card, shuffles them and draws five to form their initial hand. 

Place the objective markers on the map on their appropriate sides. The USA player controls these four objectives: St. Louis, Louisville, Lexington, and Frankfort. The CSA player controls these ten objectives: Chattanooga, Huntsville, Memphis, Nashville, Knoxville, Birmingham, Vicksburg, Corinth, Little Rock, and Jackson.

Place the units at the positions located on the map: the Generals' starting positions are shown by their name followed by the number of divisions they start with, Supply Trains by 'T'.

Each player denotes the number of divisions each General on the map has in their armies, and keeps track of this army sheet any time there are changes, such as reinforcements, casualties, or the placement of new Generals.

Sort the inactive CSA and USA Generals and place them into two opaque containers and give them to the respective players.

The USA player begins the game with the initiative, so the USA player will decides who moves first, either himself or his opponent.

After determining the start player, play begins with the first of three maneuver rounds in Turn 1.

# Playtest Notes {#sec:playtest}

* Team play variant:
  - It is possible to play the game with teams of 2 or 3 players each. One player takes the role of overall commander, the commander assigns himself and his team mates generals and supply trains at the start of the game, for which each player is individually responsible for the remainder of play. During the card draw phase divies out the cards as he sees fit amongst his team. Team members play simultaneously in the maneuver and recovery rounds, and make their own decisions.

Last edit:
6 Jan 2024
