# how to build a pdf from the markdown file using pandoc
# this uses a stale Python package for the optional filter -- pandoc-secnos -- needs to be replaced with something else
pandoc -V geometry:margin=1.5in -o test.pdf hiramgame.md --filter pandoc-secnos --number-sections
