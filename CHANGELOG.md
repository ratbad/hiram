CHANGELOG
=========
This changelog tracks major and minor versions only, not patches. 

This changelog began with v. 0.5.

0.7
---
* Changed 
    - The skill system for generals
    - Victory conditions for USA: control 10 of 14 cities or Vicksburg + 8 other cities
    - Riverine movement rate for USA player reduced to six cities per activation
    - Reduced cavlary units to 2 for CSA and 1 for USA
    - Costs for cavalry screens: 1st is free, 2nd (CSA only) is 1 TC of any value
    - A single cavalry unit blocks supply trace in all adjacent cities
    - Initiative rule: USA always goes first in each maneuver round

* Added 
    - A new system of command cards for activating units for movement and combat
    - A lack of aggression penalty for both sides that can increase or decrease the game length

* Removed 
    - Skill based TC bonus draws prior to combat
    - Zones of control

0.6
---
* Changed
    - Cavalry screen cancellation rules: 1) higher skill 2) in friendly territory 3) has a cav unit to spare
    - CSA draws 8 TC instead of 7
    - Both sides pay 5 TC each for each unit in Recovery Rounds 
    - Game length modifications standardized to one turn each 
    - Game length reduced to 8 at start, 10 max 
    - Each USA level-3 General above two shortens game by one turn 
    - Each USA level-0 General introduced lengthens game by one turn
    - Supply throw distance decreases from 6 cities to 5 cities 
    - Retreats are more like Friedrich/Maria: Distance = number of losses, and path is determined in alternating sequence of victor-loser
    - Placement restrictions: adjacent or next to friendly controlled objective city
    - Face-up/face-down TC discards 

* Added 
    - Supply trains may not throw supply across the Mississippi River
    - Tie breaker rules for determining initiative: 1) whoever has most TC 2) whoever controls most objectives 3) the CSA
    - OOS stack loses one TC or one division per General
    - Conquering an objective requires expending one TC of any value

0.5
---
* Added
    - zones of control for higher skilled generals
    - ability to cancel cavalry screens in some instances 
    - free cavalry screens for higher skilled generals
    - a new city on the map: Johnsonville, TN
    - additional ways the game turn length can fluctuate
    - if General Forrest comes into play, the CSA player permantently loses 1 cavalry unit 

* Changed
    - a major victory now includes instances where a skill 3 general gets beat by a skill 0 general
    - max number of generals is 6 per side
    - supply phase occurs simultaneously for both players at the beginning of each maneuver round
    - after supply phase, remainder of maneuver rounds alternates between players
    - enemy cavalry do not stop movement 

* Removed
    - detachments
    - death of CSA generals as well as the fate deck

* Fixed
    - inconsistencies with rules relating to movement and supply
