# Hiram: The American Civil War's Western Theater, 1862-1863

Rules version 0.6.3 

## Index 
* Components: @sec:components
* Victory: @sec:victory
* Map: @sec:map
* Tactical Cards: @sec:tc
* Pieces: @sec:pieces
* Sequence of play: @sec:play
* Combat: @sec:combat
* Supply: @sec:supply
* Setup: @sec:setup
* Playtest: @sec:playtest

##  Components {#sec:components}
* 1 map
* 1 counter sheet
* Not included but needed for play: 4 decks of cards.
* scratch paper (not included) for keeping track of how many divisions are in each General's army

### Counter sheet contents
* Generals
* Cavalry
* Supply Trains
* VP Markers, and other markers for General records keeping

## Foreword {#sec:fwd}
The setting for this game is the American Civil War (ACW) in a portion of the western theater, beginning around February or March of 1862. One player takes the role of the Confederat States of America (CSA), the other the United States of America (USA).

This game employs a rift of the system developed by Richard Sivel in his excellent designs *Friedrich* and *Maria*. The variation of the Sivelian system used in *Hiram* adds a few extra nuances but it is still a relatively simple game that should be possible to play to conclusion in a single session.

# Victory Conditions & Game Length {#sec:victory}

The USA player immediately wins as soon as they control 11 of the 14 objective cities. If the game ends before the USA player meets their victory condition, the CSA player wins.

Each turn in the game represents a period of time of two weeks to two months. At the beginning of the game, the game is limited to 8 turns. Certain conditions will affect the game length, and when these occur the game is decreased by a single turn or increased by a single turn. The maximum turn limit in the game is 10. 

## Conditions that shorten the game

* Any time the CSA player captures or recaptures ('liberates') an objective city, **or** wins a major victory, the turn limit marker is reduced by one turn. 

* Whenever the USA player introduces a level-3 skilled General onto the map, and the USA has at least two level-3 skilled Generals already on the map, the game is increased by additional turn. 

*Example: in a Maneuver round, Grant and Thomas are on the map, the USA player fires Pope for losing a major battle and replaces him with McPherson, the game is immediately shortened by one turn, from 8 turns to 7 turns. In the Recovery Round of that turn, the USA player brings Sherman on to the map as well which would be the fourth skill level-3 General for the USA, so the game length is immediately reduced again by a single turn, from 7 turns to 6 turns.*   

## Conditions that lengthen the game

* If the USA player wins a major victory, the turn marker limit is increased by one turn. *NOTE:* The game length is not extended when the USA captures/recaptures an objective.

* If the USA player introduces a level-0 skilled General on to the map the game is increased by one turn. 

* Any time there are **three** CSA cavalry units in the cavalry screen box, the game length is increased by one turn.

# The Map {#sec:map}
The map consists of cities connected by lines of communication along which the pieces move. There are three types of lines of communications: road, rail, and riverine. Some of the cities are also objectives, which the players vie for control of in order to win the game. The map grid corresponds to different suits of cards, which determine the trump suit for combat. 

* Cities may be neutral, friendly, or enemy/hostile. This status of cities is depicted by their symbol. Cities depicted with blue circles are friendly to the USA, cities of orange squares are friendly to the CSA, and cities of white triangles are neutral. The colored surfaces of the map correspond to these colors. 

* Cities friendly to the USA are hostile to the CSA and vice versa. 

# Tactical Cards (TC) {#sec:tc}
TC are the game's currency: players use TC to conduct combat, avoid combat (see cavalry screens), or recruit troops. 

Every TC has a suit (hearts, clubs, diams, spades) and a value from 2 to 10.

There is a special wild card type called "Reserve". When a player plays a Reserve card, he declares it as being any suit and any value from 1 to 8. _NOTE:_ Declaring the value is of great use in minimizing battle losses. 

# Pieces {#sec:pieces}
There are three types of pieces in the game: Generals, Cavalry, and Supply Trains.

## Generals
Players move and fight with their Generals. Each player has a supply of Generals, some of whom start on the board and the other in the Available Generals box. 

* A General may be discarded from the game, either by being fired, or when they cannot satisfy retreat requirements, or in the case of skill level 0 Generals only, when they lose a battle. When this happens the General is removed from the map and is not available for play for the remainder of the game.

* Generals have a skill rating of 0 to 3.

* A player can never have more than 6 Generals on the map at any time.

* Each General must command at least one division. 

* The maximum number of divisions a USA General may command is 10, and the maximum a CSA General may command is 8. 

### Stacks of Generals 
  * Normally, only one General may ever be placed on each city. **Exception:** Two Generals may occupy one city to form a stack. In this case, the General with the higher skill rating is the overall commander and is placed on top of the stack. The overall commander's skill rating is used for combat and movement purposes. If two Generals have the same skill rating, the player decides which General is the overall commander. 

  * When a player forms a stack, movement is finished that maneuver round for both Generals that form the stack. 
  
  * In a stack, Generals treat the sum of their divisions as a common value. Each stack must contain at a minimum two divisions, with a maximum of 20 divisions for a stack of USA Generals or 16 divisions for a stack of CSA Generals. 

  * As long as his Generals are stacked, a player can transfer brgiades between his Generals whenever he desires. He can do it even during the active turn of the other other player. 

  * A General who loses his last division is removed from the board, unless the General is in a stack and it is possible to transfer at least one troop to him. 

  * Generals may dissolve the stack at the beginning of the movement phase and move separately. 

### How Generals Move
* Generals move along the board from city to city using a line of communication - either rail, road, or river. A General may move up to the movement allowance, even back and forth.

* A General can only utilize one type of movement rate, it cannot mix different movement rates. For example, Generals may only use railway movement if the entirety of the move is conducted on a railroad, i.e., they start and stop movement on a railroad and likewise, USA Generals may only use river movement if beginning and ending on a river space. 

* Generals can move up to four cities by road, or eight cities by rail. Also, only one USA General per maneuver phase mave move up to eight cities by river. **Exception:** Generals on either side with a skill rating of zero can only use half of the normal movement allowance for all the different types of movement. 

* **CSA Generals may move an additional city if** the following conditions are met: 
  1. The cities along the line of movement are all friendly 
  2. The cities along the line of movement do not include enemy controlled objectives 

* Generals may never move onto or through enemy Generals or friendly supply trains. 

* Generals may move onto an enemy cavalry unit and or enemy supply train and continue moving. If a General moves onto an enemy supply train it is considered to be captured, that supply train is removed from the board, and the player who captured it immediately gets a bonus draw of a single TC.

#### The Mighty Mississippi 
* The Mississippi River is an obstacle that may only be crossed at the ten locations designated on the map. 

* Crossing the Mississippi consumes the entire movement allowance of a General or a Supply Train. To cross the Mississippi, a General or Supply Train must begin adjacent to the Mississippi. Upon reaching the far side after crossing the Mississippi, the  General or supply Train ceases movement for the duration of the maneuver phase. 

* Pieces on opposite sides of the Mississippi are not adjacent. 

* Supply trains cannot throw supply across the Mississippi River.

* _NOTE_: St. Louis does not have a Mississippi connection, so it and its connections are treated normally, and the above rules concerning adjaceny, movement, and supply do not apply to it. 

### Zones of Control

* Generals only project a 'zone of control' extending into cities adjacent by road or rail connections. 

* Zones of control only matter during the movement phase of the maneuver round, they do not matter when retreating. 

* If a General has a **lower skill rating** than an opposing General, then it must stop and end its turn when _entering_ a city in an enemy zone of control. However, Generals beginning the maneuver round in a valid enemy zone of control may exit that zone of control and move as usual. 

### Conquering objectives 

* Control of objectives changes due to conquest. Only a face-up General may conquer an objective. An objective is conquered when:
  * A face-up General moves out of the objective either by moving through it or by starting its move on it and moving away and 
  * ...the objective is not protected at that moment. 

* An objective is protected if a General of the side currently in control of the objective is positioned within 3 cities away. 

* A General can conquer more than one objective in a single move. After conquest, a conquered objective may still be entered by other pieces. 

* When an objective changes control, the conquering player must expend one TC of any value face up and then flip the marker accordingly to denote its new owner. 

* **Retroactive Conquest.** If a General moves through or away form a protected objective, put a marker on it to denote its possible conquered status. In the retroactive conquest phase of the same stage, check each objective with such a marker. If the objective is not protected anymore (due to retreats after combat), it is retroactively conquered. If the objective is still protected it is not conquered and the marker is removed.

## Cavalry
The CSA player has three cavlary units and the USA player has two cavlary units. Cavalry units can block enemy supply traces  and can enable a General to refuse combat, which is known as a cavalry screen. 

* Cavalry units do not move or participate in combat and cavalry units do not have to be in supply.

* Generals (but not supply trains) may move through enemy cavalry units. If an opposing General moves onto the enemy cavalry unit, that cavalry unit is simply returned to its owner (available for play in his next maneuver round) and the General may continue moving as usual. 

* Friendly Generals and friendly supply trains may move through friendly cavalry units. 

* The players may place their cavalry units at the end of each of their movement phases, cavalry units are placed on a vacant city up to 4 cities away by any combination of rail or road connections from a friendly General. Players are not obligated to place their cavalry, they may place some, all, or none of their cavalry.

* Players may not trace supply through a city that is occupied (also known as 'interdicted') by enemy cavalry. 

* Either player may form a stack of two cavalry units when placing them on the map. When stacked, the cavalry units' interdiction zone blocks supply traces for their current city as well as all vacant cities one road or one rail movement away. 

## Supply Trains
Supply Trains provide supply to Generals when they are in neutral or enemy territory.

Supply Trains move at a rate of 2 by road or 4 by rail, they may not utilize river movement.

Supply trains may not move onto or through a space occupied by a friendly or enemy General or enemy cavalry. They may however move through a space occupied by a friendly cavalry.

# Sequence of play {#sec:play}
Each turn consists of three maneuver rounds and one recovery round.

## Maneuver Rounds
The player with the most TC chooses if the CSA will go first in the maneuver phase and the USA second, or vice versa. In the event of a tie of TC, the player with the most objectives currently under his control chooses who goes first, and if still tied the CSA player chooses who goes first. 

Each maneuver round consists of a simultaneous supply check then a movement-cavalry-combat round.  

After a supply check, the active player reclaims cavalry from the board, conducts all movements, optionally places cavalry, then conducts combats, and after these are resolved play passes to the next player who does the same. 

After both players have acted in the maneuver round, the round is is over and players advance the marker to the next space on the round track. 

**At the beginning of each player's maneuver round, if the player has no TC remaining, the player draws 1 TC.** 

## Recovery Rounds
The Recovery round is an administrative round that both players conduct simultaneously. There is no movement or combat in this round. 

These are the components of the recovery round: 

* Draw TC
* Place Reinforcements
* Purchase supply trains and divisions
* Replace one General

### Draw TC
The **USA player draws nine TC** and discards one, the **CSA player draws eight TC** (unless modified, see below) and discards one. 

When the players exhaust their first deck, they then draw from the next deck. When both decks are exhausted they reshuffle each deck individually and repeat the process of drawing from one deck at a time. 

#### Negative TC Draw Modifiers for the CSA player
During the recovery round, the CSA player may suffer a TC draw penalty under the following cirucmstances: 

* If the USA controls both Vicksburg and Memphis the CSA player draws two less TC.

-or- 

* If the USA player controls all objectives in Tennessee (Knoxville, Nashville, Memphis, and Chattanooga), the CSA player draws two less TC.

### Place reinforcements
Each turn the CSA player receives two divisions and the USA player receives three divisions. Players keep track of these reinforcements on a sheet. These reinforcements may be used to add new Generals to the map, or to reinforce a General currently on the map, provided that General is not at the maximum division limit and that the General is face up (not out of supply). 

### Buy divisions and supply trains
Aside from the reinforcements, each player may purchase additional divisions or supply trains (collectively referred to here as units) for 5 TC each. There is no change for purchases, i.e. a TC with a value of 9 can only purchase one unit, but a TC or 9 and 6 would be able to purchase 3 units.

A player may expend some, none, or all of his TC to purchase units.  

Newly purchased divisions are placed in the same manner as reinforcements. Newly purchased supply trains can only enter the board in a vacant city adjacent to or on a vacant objective city that is currently friendly controlled.

### Replacement/Introduction Procedure for Generals

Whenever a player must add a General to the map, either by forming a new army or firing an existing General (see below), the players randomly select two Generals from the supply of their avialable Generals and choose one, the other is returned to the supply. 

* The CSA player **must** select the General with the lower skill rating, returning the higher skilled General to their supply. 
* The USA player **may** select the General with the higher skill rating, returning the other General to the supply. 

In the case of two Generals with equal skill ratings, the players choose which to keep and which to return to the supply. 

**Forrest**: If the CSA player brings General Forrest into play, they permanently lose one of their cavalry units. 

#### Firing Generals 
Players may 'fire', i.e. replace, any one General on the map during the Recovery Phase. The General being replaced does not need to be in supply. 

After taking the replacement, discard the replaced General from the game.

The replacing General takes the same position on the map as the replaced General and assumes control of all of the replaced General's divisions. 

#### Introducing additional Generals to the map

Each side is limited to 6 Generals on the map at any one time. If players have not met this limit they may inroduce a new General to the map during the Recovery Phase.

Newly arrived Generals can only enter the board in a vacant city adjacent to or on a vacant objective city that is currently friendly controlled.

# Combat {#sec:combat}

* Every General who is adjacent to an enemy General at the beginning of his combat phase must attack. If more than one attack must be made the attacking player chooses the order of resolution. **Note**: Armies are never considered adjacent by riverine connections, they are only adjacent by road and rail connections.

* An attack is resolved as a card game using Tactical Cards (TC). A player may play only those TCs which are of the same suit as the sector in which **his** General is positioned.

* If a General/stack starts the combat adjacent to more than one opponent he has to fight them one after the other. If more than one General/stack are adjacent to one oppoent, they have to attack one after the other. 

* In very rare circumstances, opposing Generals may end adjacent to each other yet neither player has any TC remaining. In this case, no combat occurs. 

## Combat sequence
1. Cavalry screens
2. Determine the suit
3. Determine Initial Score
4. Determine if there is a TC bonus draw 
5. Play TC
6. Resolve victory and retreats
7. Check for retroactive conquests

### (1) Cavalry screen
Either player may elect to retreat prior to combat by using a cavalry screen. If the General who is avoiding combat by playing a cavalry screen has a higher skill rating than his opponent General(s), this cavalry screen is free. Otherwise, if the General has the same skill or lesser skill than his opponent(s), the owning player expends a single TC from their hand of any value, **face up** in to the discard pile, then moves two cities and the battle is over. Unlike a retreat, these cities are both chosen by the owner.

* The General withdrawing using a cavalry screen may move in any direction provided he does not end adjacent to either the General or city he withdrew from. 

* During a withdrawal via a cavalry screen the defender may not conquer objectives or capture enemy supply trains. 

* If the defender did not elect to use a cavalry screen, the battle resolution continues to step 2 - determining the suit.

* Additionally, the defending General's cavalry screen may be cancelled by the attacking General under the following conditions: 
  1. The defending General is located in territory that is friendly to the attacking General 
  2. The attacking General's skill rating exceeds the defending General's skill rating
  3. The attacking player has an available cavalry unit to spare. 

  In this situation, the attacking General must expend one cavalry unit into the cavalry screen box, the defending player's cavalry unit remains in the cavalry screen box - it does not return to his hand, the defending General's retreat is cancelled, the defending General is not reimbursed for expending a TC to initiate the initial cavalry screen, and combat occurs. 

_Reminder_: If there are ever 3 CSA screens in the cavalry screen box, the game is immediately lengthened by one turn. 

### (2) Determining the suit
Each General can only conduct combat with cards matching the suit of the grid square in which the General is currently located.
If Generals are conducting combat across a grid square, they will each conduct combat in a different suit from each other. 

### (3) Determine initial score
First The opposing players state how many divisions their participating Generals command. The difference between these two numbers is called the *initial score*. This score is negative for the player inferior in divisions and positive for the other player. 

### (4) TC Bonus
Players next check to see if one side will draw a bonus TC. There are three conditions in which a player may draw bonus TC prior to resolving combat. In each case, the player who wins this bonus draw simply draws the top card from the draw deck and takes it into their hand, available for play in the ensuing combat or keep future use. The conditions for drawing TC consist of the following:

1. **Vicksburg** -- If a USA General or stack of Generals attacks any units in Vicksburg, then the CSA player draws the bonus TC. *This represents the inherent fortifications of Vicksburg.* 
 
2. **OOS** -- If at least one attacking General is out-of-supply, the defender draws the bonus TC and vice versa if a defending General is out-of-supply the attacker draws the bonus TC.

3. **Skill** -- Each player determines the highest skill rating of their Generals participating in combat. Whichever side has the single highest skill rating draws one bonus TC.

### (5) Play TC
* Next, the inferior player has the right to play a single TC of his suit. He adds the TC value to the initial score. This results in the new score which he states aloud. The score is always the same for both players, except that it is negative for one player and poisitive for the other. As long as a player has a negative score, he has the right to play another TC.

* If the player receives the right to play a TC on a score of zero, me must play card if he has any of the correct suit (he is not obligated to play a Reserve). If he has none (and he is not willing to play an existing Reserve), combat ends as a tie. In a tie, neither side loses divisions nor has to retreat. 

* Stacked Generals always fight together. 

* If a score becomes *zero or positive*, the right to play TCs switches to the other player. Now he is considered inferio and he may play a TC using the same procedure. The right to play TCs keeps switching until the player with the right to paly is unable or unwilling to do so. At that point his General is defeated. 

* If the attack starts with a score of zero, the attacking player plays TC first. 

* The defeated General loses as many divisions as the final *negative score* but no more than he commanded and retreats four spaces. Retreats apply to *defeated* Generals, whether they attacked or defended.  

* The winner loses no divisions and may either remain in place *or* advance to the space the retreating General vacated. A victorious defending General may also make this advance after combat. 

### (6) Resolve victory and retreats 
* If a General loses all of his divisions as a result of combat, the General is removed from the board and returned to the supply of available Generals **unless he has a skill level of zero** in which case the General is removed from play.  

* A General who had to rereat may not attack or be attacked again in that combat phase.

* A defeated General has to retreat before the next attack is resolved.

* During a retreat a stack may never split up. 

* **Retreat Path**: A General retreats a number of cities equal to the number of losses sustained in combat. Both players determine where the defeated General retreats. The first, third, fifth, etc. cities are chosen by the victor, the second, fouth, sixth, etc. cities are chosen by the loser. _Example: Rosecrans of the USA is defeated in battle by a total of three, Rosecrans loses three divisions, then the first city he retreats to is chosen by the CSA player, then for the second city the USA player chooses from there an adjacent city to retreat to, and finally the CSA player chooses the third city Rosecrans must retreat into_. 

* A General may not pass through any individual city more than once during a retreat. 

* Generals may not retreat through enemy Generals, enemy supply trains, or friendly supply trains, nor may they retreat across the Mississippi River. 

* If a General cannot retreat the full distance due to occupied cities or a barrier such as the map edge or Mississippi River, he loses all divisions and is placed in the Relieved of Command box.  

* **Major Victories**: If one side wins by a final TC score of 6 or more, *or* if a General with a skill level of 0 defeats a General with a skill level of 3, then the winning player achieves a "Major Victory". When this occurs, players adjust the turn length accordingly, lengthing the game by one turn if the USA won, or reducing the game length by two turns if the CSA won. Additionally, the player who loses a major victory may immediately fire their General who lost the major victory. 

### (7) Check for retroactive conquests
Retroactive conquests are checked for after all combat is executed. 

# Supply {#sec:supply}
Generals need supply. Generals check for supply at the beginning of each maneuver phase. If a General is not situated on a friendly city it must be able to draw supply from a valid supply source. 

## Supply sources 

* Supply trains: A supply train is a source of supply if it can draw supply from a friendly city that is not an enclave, or another friendly supply train that likewise satisfies these conditions. In this way, players may chain together multiple supply trains to form a network of supply lines. 

## Supply Lines
Supply trains and Generals can **draw supply from a supply train up to 5 cities away** - this forms a supply line. 

A supply line may combine road or rail movement rates only (with one exception for the USA player, see below). Supply lines cannot extend through enemy occupied cities (enemy cavalry, General, or Supply Trains) **and** may also not extend through unoccupied objective cities that have an enemy marker on them. 

**All USA Generals are also in supply if located on a city that can trace a riverine path of unlimited distance to Cairo, St. Louis, or Louisville provided that no intervening cities in this path are occupied by a CSA General or CSA cavalry unit.**

## Enclaves
Friendly cities located in predominantly enemy or neutral areas are known as enclaves.

* USA enclaves: 
  * Oneida, TN
  * Greenville, TN
  * Haleyville, AL
  * Laurel, MS
  * Marhsall, AR 

* The CSA has a single enclave in Henderson, KY. 

Players may not introduce new pieces on the map onto enclaves. 

**Note**: Enclaves may supply friendly Generals located there, but they cannot be used as a draw source for a supply train. 

## Out of supply (OOS) 
If a General cannot trace a line of supply to a friendly supply train, or is not located on a friendly city, then it is considered to be out of supply.

* If a player's supply line is blocked by an enemy cavalry unit at the beginning of his maneuver phase he may pay a single TC, worth any amount to keep his General in supply. 

* When a General cannot trace a supply path when required, he immediately runs out of supply, is turned face up, and faces two options: either 1) lose one division from his army or 2) expend a TC of any value to not take this loss.

* If a stack of two generals is out of supply the stack either 1) loses two divisions or 2) its owner expends two TC of any value to not take losses. 

* Generals who are out of supply move as normal and fight as normal but may not capture objectives. 

* If an in-supply General is fighting one or more out-of-supply Generals, then that General earns a bonus draw of a TC prior to resolving combat.

## Capturing supply trains
Supply trains may be captured by enemy Generals. 

To capture an enemy supply train, players simply move Generals onto them.

This stops the movement of the General and this removes the supply train from the board, not permanently, it may be purchased and placed again in the next recovery round. 

The capturing player then earns a bonus: the player draws a single TC per enemy supply train captured. 

# Set up {#sec:setup}
Place the turn limit marker on turn 8 and the current turn marker on turn 1. 

Prepare the TC draw deck: Take 4 decks of cards, give each player two decks. Each player has their own deck. 

Remove the Jokers, and all the face cards except for the Aces. For the CSA player only, remove all TC of value 2, 3 or 4 from their two decks, discard from the game. Then both players shuffle each deck individually. 

The CSA player draws 8 TC and discards one, the USA player draws 9 TC and discards one.  

Place the objective markers on the map on their appropriate sides. The USA player controls these four objectives: St. Louis, Louisville, Lexington, and Frankfort. The CSA player controls these ten objectives: Chattanooga, Huntsville, Memphis, Nashville, Knoxville, Birmingham, Vicksburg, Corinth, Little Rock, and Jackson.

Place the units at the positions located on the map: the Generals' starting positions are shown by their name followed by the number of divisions they start with, Supply Trains by 'T'.

Each player denotes the number of divisions each General on the map has in their armies, and keeps track of this army sheet any time there are changes, such as reinforcements, casualties, or the placement of new Generals. 

Sort the inactive CSA and USA Generals and place them into two opaque containers and give them to the respective players.

The USA player begins the game with the initiative, so the USA player will decides who moves first, either himself or his opponent. 

After determining the start player, play begins with the first of three maneuver rounds in Turn 1.

# Departures from the Sivelian system
For players that are familiar to Maria/Friedrich, this section provides a quick reference of the differences in this game, which may be helpful in getting a quick start. 

* This is a 2-player game only. 

* Supply trains chain together to form supply lines.

* Cavalry are like Hussars from Maria, but can also be used to avoid/refuse combat by setting screens which may require expending TC.

* Generals have a skill rating, 0 to 3, which effects movement and combat.  

* Some Generals project zones of control. 

* There are three types of movement: river, rail, and road, and there are no forced marches. 

* Generals stop movement when moving onto enemy supply trains but are rewarded a bonus TC for doing so. 

* There is a linear obstacle, the Mississippi River, that  effects movement/retreating rules.

* Asymmetric differences: 

  * The CSA player has stronger combat abilities, movement rates, and has a cavalry advantage. 
  
  * The USA player has better chances of getting higher skilled Generals on the map, can use river momement, and has a material advantage in larger army sizes, more TC, and more reinforcements. 

# Playtest Notes {#sec:playtest}

* After turn 1, if a side did not initiate an attack during the turn they are penalized. USA gets penalized with a one turn decrease, CSA with a one turn increase. 

* When gaurding an objective (defending within 3 spaces), TC values of 5 and below are wild.

* USA gets a General limit of 7 instead of 6. 

* Replace supply trains with dice: 
  - CSA gets x6 D10, starts with 3 
  - USA gets x7 D10, starts with 4
  - Supply checks still occur according to the old rules: a supply trace at the beginning of the maneuver round which gets resolved immediately. This represents all other classes of supply other than ammo (class V).
  - However, now when in battle, either attacking or defending, each card play requires a rotation of a single pip/die face - representing Class V supply (ammo). These rotations/supply expenditures may be taken from any dice in the supply chain. 
  - These dice throw supply at a distance of 4 but also move at a rate of 4. 
  - When captured, the dice do not yield a TC draw but instead the owner gets to keep either A) all of the rotations on the pip or B) half of the rotations rounded up left on the dice... and transfer them to their network  
  - Dice are introduced onto the board with their full supply points, and during the recovery phase, a player buy more supply poitns for their dice, when doing so rotate die faces equal to the value of TC expended, for example, the USA player expends a 2, a 6, and a 9 for a total of 17, and may then rotate dice up to a total of 17 pips. 
- Stacks are now increased to a limit of 3, however, each stack must expend 1 pip per general in their stack each time they play a TC in battle. Example: a stack has 3 generals versus a a single general, the stack must turn 3 pips to play their card, while the single general only expends a single pip to play a card on his turn. 

* Team play variant: 
  - It is possible to play the game with teams of 2 or 3 players each. One player takes the role of overall commander, the commander assigns himself and his team mates generals and supply trains at the start of the game, for which each player is individually responsible for the remainder of play. During the card draw phase divies out the cards as he sees fit amongst his team. Team members play simultaneously in the maneuver and recovery rounds, and make their own decisions.

* Event cards. At the beginning of the game randomly select two cards to be placed on the political display. From then on, after these are replaced, each player chooses one card to go on the display next. On the political display. These are auctioned off during the Recovery phase only using the trump suit - which is the suit of whoever most recently won a battle with. Like in Maria players may bluff with non-trump suit cards. The winner loses his card and chooses if the two event cards take place or not. The loser leaves their cards on the display until the next turn, unless they weren't in the trump, those are returned to their hand.  

  - Grant Succumbs to his Demons - Grant gets excessively drunk and performs at skill level 0 until the next recovery round. 

  - Anaconda Plan- CSA only draws 1/2 amount of cards in next resupply phase 

  - Factory Strikes in Northeast- USA can’t purchase supply trains or supply points in next recovery phase 

  - Indian Uprising- must have at least one general west of Mississippi at the end of this turn or the turn count is increased/decreased 

  - Battlefield Surgery- red suit cards decreased by one point for combat only for the remainder of the game. 

  - Confederate Rail Gauges - units move half their normal RR distance for the remainder of the game 

  - Mississippi Floods- units can’t cross Mississippi River this turn 

  - Telegraph lines cut- Generals only defend objectives within two spaces 

  - Gatling gun- black suit cards are interchangeable when defending for owner in the next turn 

  - Grant’s Gunboats- Aces wild for USA player if on a river city during combat (seems to conflict with adjacency rules?)

  - Battle of Vicksburg- Vicksburg counts as three objectives if captured this round by USA

  - Stronghold: All TC are wild for CSA Generals within 3 spaces of Vicksburg either attacking or defending. 

  - Confederate desperation - CSA may purchase brigades and supply trains for 4 points instead of five for the remainder of the game.

  - Cleburne Emancipation - if Cleburne is on the board at this moment, the CSA player gets +1 reinforcements in every recovery round for the remainder of the game and +4 reinforcements (one time effect) to be placed immediately. 

  - Longstreet detached from AoNVA: Longstreet (skill level 3) arrives in this recovery round with 4 divisions under his command. These divisions may not be transferred to another General and may not be reinforced. Longstreet arrives in the same manner as other Generals in or adjacent to friendly controlled objective. Longstreet may be the 7th general for the CSA. Longstreet and his remaining divisions are immediately removed from play in the next recovery round. 

  - Leader casualties: Any of these generals on the board at this moment: CF Smith, McPherson (USA) or Cleburne, Polk, AS Johnston (CSA) are are immediately relieved of command according to the normal rules. 

  - Promotions: USA player may immediately replace any general on the board with Sheridan, McPherson, or Sherman; CSA player may immediately replace any general on the board with J. Johnston, Forrest, or Cleburne. 

  - Jefferson Davis Reconsiders Strategy: The CSA player may move any objective markers that began the game in CSA control a combined a total of 5 cities (except Vicksburg), and they remain there for the remainder of the game. Also these objective markers may not cross state boundaries, i.e., all TN markers must remain in TN, etc. 

  - International pressure: if the USA player does not presently control Vicksburg, the game length is reduced by 2 turns. 

  - Politically appointed generals: the winner of this event gets to fire any General of his opponent (except for Longstreet if on the board) and replace him with the General of his choice. 

  - War shortages: The winner of this event card gets a free supply train placement, which may be in excess of their limit, and gets to remove one supply train of their choice belonging to their opponent.

* A different turn structure: Instead of three maneuver rounds with a you go - I go sequence this version would use a passing system. At the beginning of the manuever round there is a simultaneously supply check for all players, then beginning with the USA player, players have the option to move one general and/or one supply train on their turn, resolving combat at the end of movement. Alternatively the player may pass, and if so they get to draw 1 TC, then play passes to their opponent. When both players pass in succession, the maneuver round is over and play proceeds to the recovery phase. The recovery phase remains the same. The game begins with 4 cards with the USA and 3 cards for the CSA. 

Last edit:
19 Feb 2024
