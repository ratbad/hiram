# hiram

Play-test files for Hiram, a two-player American Civil War implementation of the Friedrich system created by Richard Sivel. 

The most current rules of the game are usually in the markdown file not the PDF version.

Map designed with `QGIS`, all data the map were digitized from free open sources and are original. `pdfposter` used for splitting the poster into multiple 8.5x11 inch paper for home printing. 

Rules developed with `pandoc`.
